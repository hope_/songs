import React, { Component } from "react";
import "../styles/App.css";
import SongList from "./SongList";
import SongDetail from "./SongDetail";

class App extends Component {
  state = {};
  render() {
    return (
      <div className="app-container">
        <SongList />
        <SongDetail />
      </div>
    );
  }
}

export default App;
