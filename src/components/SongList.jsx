import React, { Component } from "react";
import { connect } from "react-redux";
import { selectSong } from "../actions";
import "../styles/SongList.css";
class SongList extends Component {
  renderList() {
    return this.props.songs.map((song) => {
      return (
        <div className="song-wrapper" key={song.title}>
          <div className="song-title">{song.title}</div>
          <button
            className="select-button"
            onClick={() => {
              this.props.selectSong(song);
            }}
          >
            Select
          </button>
        </div>
      );
    });
  }
  render() {
    return <div className="song-list-wrapper">{this.renderList()}</div>;
  }
}
const mapStateToProps = (state) => {
  return { songs: state.songs };
};

export default connect(mapStateToProps, { selectSong })(SongList);
