import { combineReducers } from "redux";

const songsReducer = () => {
  return [
    { title: "Destiny", duration: "3:10" },
    { title: "Pull me apart", duration: "1:50" },
    { title: "Cold", duration: "2:57" },
  ];
};

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === "SONG_SELECTED") {
    return action.payload;
  }

  return selectedSong;
};

export default combineReducers({
  songs: songsReducer,
  selectedSong: selectedSongReducer,
});
